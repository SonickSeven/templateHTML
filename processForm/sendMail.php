<?php
	if(isset($_POST['name'], $_POST['mail'], $_POST['subject'], $_POST['phone'], $_POST['message'])){
		$template = file_get_contents("./templateMail.html");
		foreach($_POST as $key => $value){
			$template = str_replace('{{ '.$key.' }}', $value, $template);
		}
		$headers="MIME-VERSION: 1.0 \r\nContent-type: text/html; charset=UTF-8\r\nFROM : <".$_POST['mail'].">\r\n";
		if(mail('fundacionsemillas@yahoo.com', $_POST['subject'], $template, $headers)){
			echo json_encode(array(
					'err'=>false,
					'msg'=>'Se envio correctamente el mensaje'
				));
		}else{
			echo json_encode(array(
					'err'=>true,
					'msg'=>'No se pudo enviar el correo, intentalo más tarde',
					'code'=>'68se4rg6drtg8hrdth'
				));
		}
	}else{
		echo json_encode(array(
				'err'=>true,
				'msg'=>'Las algunas de las variables de envio estan vacias',
				'code'=>'6s5e4rg6se4g8er'
			));
	}

?>