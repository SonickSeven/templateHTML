function si(tag, cb){
	[].forEach.call(document.querySelectorAll(tag), elem=>{
		if(cb)
			cb(elem)
	})
	return document.querySelectorAll(tag).length
}
function se(tag){
	return document.querySelector(tag)
}

const mainvar={
	logic:{
		ajax: data=>
			new Promise(resol=>{
				xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if (this.readyState == 4) {
						if (this.status == 200)
							resol(this.responseText)
						else(this.status == 404)
							resol('No se encontro la página')
					}
				}
				xhttp.open('GET', data.url, true)
				xhttp.send()
			})
		,
		includeHTML:()=>{
			let z, i, elmnt, file, xhttp;
			si(`.ss-includes-html`, elmnt=>{
				file=elmnt.getAttribute('ss-include-html')
				if(file){
					mainvar.logic.ajax({url: file})
					.then(resp=>{
						elmnt.innerHTML=resp
						elmnt.removeAttribute('ss-include-html')
					}).catch(console.log)
				}
			})
		}
	}
}

document.addEventListener('DOMContentLoaded', mainvar.logic.includeHTML,false)