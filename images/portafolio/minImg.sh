maxWidth=1200
for img in *.JPG ; do
        widthImg=$(identify -format %w "$img")
        if [ "$widthImg" -gt "$maxWidth" ] ; then
                echo "cambiar $img"
                $(convert $img -resize "1200x1200" -quality 76 "t_"$img)
        fi
done
