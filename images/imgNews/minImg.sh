maxWidth=300
for img in *.jpg ; do
        widthImg=$(identify -format %w "$img")
        if [ "$widthImg" -gt "$maxWidth" ] ; then
                echo "cambiar $img"
                $(convert $img -resize "300x300" -quality 76 "t_"$img)
        fi
done
